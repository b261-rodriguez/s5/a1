package com.zuitt.activity;

public class Contact {
	//Instantiate of contacts information
	private String name;
	private String contactNumber;
	private String address;

	//instantiate empty constructor for contacts
	public Contact (){}

	//instantiate parameterized constructor for contacts information
	public Contact (String name, String contactNumber, String address){
		this.name = name;
		this.contactNumber = contactNumber;
		this.address = address;
	}

	//Getters
	public String getName() {
		return name;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public String getAddress() {
		return address;
	}

	//Setters
	public void setName(String name){
		this.name = name;
	}
	public void setContactNumber(String contactNumber){
		this.contactNumber = contactNumber;
	}
	public void setAddress(String address){
		this.address = address;
	}




}
