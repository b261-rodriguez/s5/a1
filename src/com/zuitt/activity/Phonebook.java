package com.zuitt.activity;

import java.util.ArrayList;

public class Phonebook {
	//instantiate contacts arrayList
	private ArrayList<Contact> contacts;

	//instantiate phoneBook for empty constructor
	public Phonebook(){}

	//instantiate phoneBook parameterized constructor
	public Phonebook(ArrayList<Contact> contacts){
		this.contacts = contacts;
	}



	//Getter
	public ArrayList<Contact> getContacts(){
		return this.contacts;
	}

	//Setter
	public void addContact(ArrayList<Contact> contacts){
		this.contacts = contacts;
	}



}
