import com.zuitt.activity.*;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {
	public static void main(String[] args) {

		//Instantiate the Phonebook
		Phonebook myPhonebook = new Phonebook();

		//create new contacts
		Contact contact1 = new Contact("Jose", "09123445","Mandaluyong");
		Contact contact2 = new Contact("Maria", "09123425","Marikina");

		//Create an arrayList of Contacts
		ArrayList<Contact> contactList = new ArrayList<Contact>();
		contactList.add(contact1);
		contactList.add(contact2);

		//Adding the arrayList of Contacts to the Phonebook
		myPhonebook.addContact(contactList);

		//Instantiate getting contacts on the phonebook
		ArrayList<Contact> contacts = myPhonebook.getContacts();

		//Checking if the Phonebook has contacts
		if (contacts.isEmpty()){
			System.out.println("No Contact Available");
		} else { //If Phonebook is not empty
			for (Contact contact : contacts) {
				System.out.println("--------------------------------");
				System.out.println(contact.getName());
				System.out.println("--------------------------------");
				System.out.println(contact.getName() + " has the following registered number:");
				System.out.println(contact.getContactNumber());
				System.out.println(contact.getName() + " has the following registered address:");
				System.out.println("My home is in " + contact.getAddress());

			}

		}
























	}
}